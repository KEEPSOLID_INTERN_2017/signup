<?php
/**
 * Created by PhpStorm.
 * User: alexandrmazur
 * Date: 22.07.17
 * Time: 12:00
 */

namespace App\Http\Controllers\Api;

use App;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\User;
use App\Services\SendMailService;

class SignUpController extends Controller
{
    public function signUpUser(Request $request)
    {
     /*   /user/sign-up:
    post:
      tags:
        - 'Sign-Up API'
      summary: 'Sign-Up a user'
      consumes:
        - multipart/form-data
      parameters:
        - name: email
          type: string
          in: formData
          required: true
    - name: password
          type: string
          in: formData
          required: true
    - name: name_first
          type: string
          in: formData
          required: true
    - name: name_last
          type: string
          in: formData
          required: true
      responses:
        200:
          description: 'Sign-Up OK'
          schema:
            $ref: '#/definitions/User'
        302:
          description: 'User exists'
          schema:
            $ref: '#/definitions/Message'
        400:
          description: 'Invalid params passed'
          schema:
            type: array
        items:
              $ref: '#/definitions/Property'
definitions:
  Property:
    type: object
    properties:
      name:
        type: string
      message:
        type: string
  Message:
    type: object
    properties:
      code:
        type: integer
      message:
        type: string
  User:
    type: object
    properties:
      id:
        type: integer
      email:
        type: string
      name_first:
        type: string
      name_last:
        type: string*/
        $requestParams = $request->only(
            'email',
            'first_name',
            'last_name',
            'password'
        );

        $validationParams = [
            'email' => 'required|email|unique:emails,email',
            'first_name' => 'max:50|min:3',
            'last_name' => 'max:50|min:3',
            'password' => 'required|min:8'
        ];

        $validator = Validator::make(
            $requestParams,
            $validationParams
        );

        if ($validator->fails()) {
            $error = [];
            foreach ($validator->errors()->keys() as $keys)
            {
                foreach ($validator->errors()->get($keys) as $message)
                {
                    array_push($error,['name'=>$keys,'message'=>$message]);
                }
            }

            return (new Response(
                json_encode($error)
                , 400, ['description' => 'Invalid params passed'])
            );

        }

        $userParams = [
            'first_name' => $requestParams['first_name'],
            'last_name' => $requestParams['last_name'],
            'admin' => 0
        ];

        $existUser = DB::table('users')->where('first_name','=',$requestParams['first_name'])
            ->where('last_name','=',$requestParams['last_name'])
            ->get();

        if (count($existUser) > 0) {
            return (new Response([
                'code' => 302,
                'message' => 'User exists'
            ], 302, ['description' => 'User exists']));
        }
        $user = new User($userParams);
        $user->save();

        $email = $user->emails()->create([
            'user_id' => $user->id,
            'email' => $requestParams['email'],
            'main' => true
        ]);

        $email->password()->create([
            'email_id' => $email->id,
            'password' => md5($requestParams['password'])
        ]);

        //app('MailService')->sendRegistrationMail($email);

        return response(new Response([
            'id' => $user->id,
            'email' => $requestParams['email'],
            'name_first' => $requestParams['first_name'],
            'name_last' => $requestParams['last_name']
        ], 200, ['description' => 'Sign-Up OK']));
    }
}
