<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class User extends Model
{
    use Notifiable;

    protected $table = 'users';

    public $timestamps = false;

    protected $appends = ['email', 'main'];

    protected $fillable = ['first_name', 'last_name', 'admin'];

    public function emails()
    {
        return $this->hasMany('\App\Models\Email');
    }

    public function getEmailAttribute()
    {
        return $this->emails()
            ->where('user_id', '=', $this->id)
            ->where('main', '=', true)
            ->first()
            ->email;
    }
}
