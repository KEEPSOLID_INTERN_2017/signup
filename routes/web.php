<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/signup', function () {
    return \Response::make('///////signup//////////',422);
});
$router->group(['prefix' => '/signup/v1/user'], function ($router) {
    // Регистрация пользователей...
    $router->post('/sign-up', 'Api\SignUpController@signUpUser');
});
